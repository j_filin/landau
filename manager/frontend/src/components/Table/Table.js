import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import CustomTableWrapper from './TableWrapper.js'
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import ReactAudioPlayer from 'react-audio-player';
// core components

class CustomTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {data: []};
        this.props = props;
        this.getData();
    }

    getData() {
        const {tableHead, tableData, tableHeaderColor} = this.props;
        if (typeof tableData == 'function') {
            try {
                tableData()
                .then(response => {
                    try {
                        if (response.ok) {
                            return response.clone().json();
                        } else {
                            this.setState({'data': []});
                        }
                    } catch (e) {
                        this.setState({'data': []});
                    }
                })
                .then((data) => {
                    const dataResult = [];
                    if (data) {
                        data.map((val, key) => {
                            dataResult.push([
                                val['type'],
                                val['code'],
                                val['value'] || val['text'],
                                val['last_check'] || val['datetime'],
                                val['text'] ? '' : (val['active'] ? 'active' : 'disabled'),
                                val['file_path'],
                            ])
                        });
                    }
                    this.setState({'data': dataResult});
                });
            } catch (e) {
                this.setState({'data': []});
            }
        } else {
            this.state.data = tableData;
        }
    }

    render() {
        const classes = this.props.classes;
        const {tableHead, tableData, tableHeaderColor} = this.props;


        return (
            <div className={classes.tableResponsive}>
                <Table className={classes.table}>
                    {tableHead !== undefined && this.state.data.length ? (
                        <TableHead className={classes[tableHeaderColor + "TableHeader"]}>
                            <TableRow className={classes.tableHeadRow}>
                                {tableHead.map((prop, key) => {
                                    return (
                                        <TableCell
                                            className={classes.tableCell + " " + classes.tableHeadCell}
                                            key={key}
                                        >
                                            {prop}
                                        </TableCell>
                                    );
                                })}
                            </TableRow>
                        </TableHead>
                    ) : null}
                    <TableBody>
                        {this.state.data.map((prop, key) => {
                            return (
                                <TableRow key={key} className={classes.tableBodyRow}>
                                    {prop.map((prop, key) => {
                                        return (
                                            <TableCell className={classes.tableCell} key={key}>
                                                {prop === 'active' ? (
                                                    <span className={'green-circle'}></span>
                                                ) : (prop === 'disabled' ?
                                                    <span className={'red-circle'}></span> :
                                                    (
                                                    prop && prop.indexOf('.wav') > -1 ? (<ReactAudioPlayer
  src={'http://104.248.27.17/' + prop}
  controls
                                                    />) : <div dangerouslySetInnerHTML={{__html:prop}} ></div>
                                                ))}
                                            </TableCell>
                                        );
                                    })}
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </div>
        );
    }

    componentDidMount() {
        this.interval = setInterval(() => this.getData(), 10000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }
}

CustomTable.defaultProps = {
    tableHeaderColor: "gray"
};

CustomTable.propTypes = {
    tableHeaderColor: PropTypes.oneOf([
        "warning",
        "primary",
        "danger",
        "success",
        "info",
        "rose",
        "gray"
    ]),
    tableHead: PropTypes.arrayOf(PropTypes.string),
    tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string))
};


CustomTable = CustomTableWrapper(CustomTable);

export default CustomTable;