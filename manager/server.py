from app import app, models, db
import json, subprocess, os, datetime, pika
from flask import request, Response
from dockermanager.compose import Compose
from elasticsearch import Elasticsearch
import json
from urllib.request import urlopen


compose_file_path = '../docker/docker-compose.yml'

@app.route("/")
def root():
    return "test"


@app.route("/services/list")
def streams():
    connection = db.session.connection()

    # services = connection.execute('select s.type, s.code, s.last_check, s.value from (select * from service_history order by last_check asc) s group by type, code')
    services = connection.execute('''
    with _services as (
      select
        *,
        row_number() over (partition by code, type order by last_check desc) as row_number
      from service_history
    )
    
    select s.type, s.code, s.last_check, s.value
    from _services as s
    where row_number = 1 order by s.last_check desc
    ''')

    res = []
    for u in services:
        type, code, last_check, value = u
        # last_check = datetime.datetime.strptime(last_check, '%Y-%m-%d %H:%M:%S.%f')
        if last_check > (datetime.datetime.now() - datetime.timedelta(hours=1)):
            stream = models.Stream.query.filter_by(title=code).first()
            res.append({
                'type': type,
                'code': code,
                'value': stream.url if stream else '',
                'last_check': last_check.strftime('%d.%m.%Y %H:%M:%S'),
                'active': last_check > (datetime.datetime.now() - datetime.timedelta(seconds=30))
            })

    active = False
    try:
        data = urlopen("http://localhost:9200").read()
        data = str(data)
        active = data.find('cluster_uuid') > -1
    except:
        print("Urlopen error")

    res.append({
        'type': 'elasticsearch',
        'code': 'main_cluster',
        'value': '',
        'last_check': datetime.datetime.now().strftime('%d.%m.%Y %H:%M:%S'),
        'active': active
    })

    res.append({
        'type': 'postgres',
        'code': 'main_database',
        'value': '',
        'last_check': datetime.datetime.now().strftime('%d.%m.%Y %H:%M:%S'),
        'active': True
    })

    active = False
    try:
        credentials = pika.PlainCredentials('guest', 'guest')
        parameters = pika.ConnectionParameters('localhost',
                                               5672,
                                               '/',
                                               credentials)
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        active = True
    except:
        pass

    res.append({
        'type': 'rabbitmq',
        'code': 'main_cluster',
        'value': '',
        'last_check': datetime.datetime.now().strftime('%d.%m.%Y %H:%M:%S'),
        'active': active
    })

    return Response(json.dumps(res), mimetype='application/json')


@app.route("/services/recognition")
def recognition():
    connection = db.session.connection()

    # services = connection.execute('select s.type, s.code, s.last_check, s.value from (select * from service_history order by last_check asc) s group by type, code')
    services = connection.execute('''
    select s.code, s.text, s.date_start, s.file_path
    from recognized_text as s
    order by s.date_start desc
    ''')

    res = []
    for u in services:
        code, text, date_start, file_path = u
        # last_check = datetime.datetime.strptime(last_check, '%Y-%m-%d %H:%M:%S.%f')
        if date_start > (datetime.datetime.now() - datetime.timedelta(hours=1)):
            res.append({
                'code': code,
                'text': text,
                'file_path': file_path,
                'datetime': date_start.strftime('%d.%m.%Y %H:%M:%S')
            })

    return Response(json.dumps(res), mimetype='application/json')


@app.route("/api/v1/stop/", methods=["POST"])
def stop():
    title = request.json.get('title')
    url = request.json.get('url')
    result = {'success': 1}

    title = title.replace(r'[^A-z0-9]', '')

    if title:
        stream = models.Stream.query.filter_by(title=title).first()

        if not stream:
            return ''

        result['data'] = stream.id
        yaml = Compose(compose_file_path)

        if stream.service_stream_name not in yaml.get_services():
            stream_reader = {
                'build': './ffmpeg',
                'container_name': stream.service_stream_name,
                'working_dir': '/landau/listener',
                'volumes_from': ['source'],
                'links': ['db'],
                'restart': 'always',
                'entrypoint': '',
                'command': ""
            }

            yaml.add_service(stream.service_stream_name, stream_reader)
            yaml.save()

        yaml.stop_service(stream.service_stream_name)
        yaml.remove_service(stream.service_stream_name)
        yaml.save()

    return result

@app.route("/api/v1/stream/", methods=["POST"])
def stream():
    title = request.json.get('title')
    url = request.json.get('url')
    result = {'success': 1}

    title = title.replace(r'[^A-z0-9]', '')

    if not os.path.isdir('../files/'+title):
        os.mkdir('../files/'+title)

    if not os.path.isdir('../ongoing/' + title):
        os.mkdir('../ongoing/'+title)

    if not os.path.isdir('../archive/' + title):
        os.mkdir('../archive/'+title)

    if url and title:
        stream = models.Stream.query.filter_by(title=title).first()

        if not stream:
            stream = models.Stream(
                title=title,
                url=url
            )
            db.session.add(stream)
        else:
            stream.url = url

        db.session.commit()
        compose_name = 'stream_reader_' + str(stream.id)
        stream.service_stream_name = compose_name
        db.session.commit()

        result['data'] = stream.id
        yaml = Compose(compose_file_path)
        exists = False


        for k in yaml.get_services():
            if k == compose_name:
                exists = True

        if not exists:
            stream_reader = {
                'build': './ffmpeg',
                'container_name': 'landau_stream_reader_' + str(stream.id),
                'working_dir': '/landau/listener',
                'volumes_from': ['source'],
                'links': ['db'],
                'restart': 'always',
                'entrypoint': '',
                'command': str("bash -c 'chmod +x run.sh && ./run.sh " + title + " \"" + url + "\" " + "2'")
            }

            yaml.add_service(compose_name, stream_reader)

            yaml.save()

        yaml.run_service(compose_name)

    return result


if __name__ == "__main__":
    yaml = Compose(compose_file_path)
    yaml.down()
    yaml.clear()
    yaml.save()
    yaml.up()

    app.run(host='0.0.0.0', port="8080")
