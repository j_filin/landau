#!/usr/bin/env python3
import pika, time, datetime, os, json, shutil, psycopg2
credentials = pika.PlainCredentials('guest', 'guest')
parameters = pika.ConnectionParameters('rabbit',
                                       5672,
                                       '/',
                                       credentials)

last_file = {}
i = 0

try:
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue='for_recognition')
    conn = psycopg2.connect(dbname='landau', user='postgres',
                            password='password', host='db')
    while True:
        time.sleep(2)
        last_nums = {}
        aFiles = {}
        cursor = conn.cursor()
        cursor.execute('insert into service_history (value, code, type, last_check) values (%s, %s, %s, %s)',
                       (
                           0,
                           'main_cluster',
                           'task_manager',
                           datetime.datetime.now()
                       ))
        conn.commit()

        for r, d, f in os.walk('/files'):
            for file in f:
                if file.endswith('.wav'):
                    file_name = file.replace('.wav', '')
                    file_obj = file_name.split('_')
                    num = int(file_obj.pop())
                    # source = file_obj[0]

                    source = r.split('/').pop()
                    if source not in aFiles:
                        aFiles[source] = []

                    aFiles[source].append({
                          'file': os.path.join(r, file),
                          'source': source,
                          'num': num
                      })


        if aFiles:
            for k in aFiles:
                aFile = aFiles[k]
                if aFile:
                    aFile.sort(key=lambda x: x['num'])
                    print(aFile)
                    aFile.pop()
                    if len(aFile):
                        cursor.execute(
                            'insert into service_history (value, code, type, last_check) values (%s, %s, %s, %s)',
                            (time.time(), k, 'stream', datetime.datetime.now())
                            )
                        conn.commit()
                        for fileObj in aFile:

                            file_mtime = datetime.datetime.fromtimestamp(os.path.getmtime(fileObj['file']))

                            fileObj['date'] = file_mtime.strftime("%Y-%m-%d %H:%M:%S")
                            arFile = fileObj['file'].split('/')
                            arFile[1] = 'ongoing'
                            arFile[len(arFile)-1] = (str(time.time()).replace('.', '')) + '_' + arFile[len(arFile)-1]

                            print(json.dumps(fileObj))
                            if shutil.move(fileObj['file'], '/'.join(arFile)):
                                fileObj['file'] = '/'.join(arFile)

                                channel.basic_publish(exchange='',
                                                      routing_key='for_recognition',
                                                      body=json.dumps(fileObj))

        cursor.close()

except pika.exceptions.AMQPConnectionError:
    print("listener: Waiting for rabbit server")